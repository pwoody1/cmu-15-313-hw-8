README:

We have made significant progress implementing our plugins.  We are, however, still waiting on some of the Framework code to be implemented (namely, the remaining empty interfaces) and as such do not yet have a working version of our code.

Furthermore, we have decided to postpone writing any tests because for one, they would not be runnable yet (due to incomplete framework) and we anticipate we will have to make changes to our code to fit the changed framework when it becomes available.