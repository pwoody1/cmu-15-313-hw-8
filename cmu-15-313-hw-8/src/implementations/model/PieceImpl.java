package implementations.model;
import java.awt.Point;
import java.awt.image.BufferedImage;
import interfaces.model.*;
import interfaces.plugins.UIPlugin;


/**
 * This class is intended to define a Piece that would be on the board of the
 *   game. Examples of pieces include knights in chess or infantry in risk.
 *   Generally speaking the only information that is known about a piece 
 *   without knowing which game its from is which player it belongs to and its
 *   current location on the board. Any other functionality such as health of
 *   the Piece, or the specific moves its allowed to make would be reserved for
 *   an instantiation of the class.
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 * 
 */
public class PieceImpl implements Piece {

	private Player player;
	private String pieceType;
	private BufferedImage image = null;
	
	private UIPlugin uiPlugin = null;
	
	public PieceImpl(Player player)
	{
		this(player, null);
	}
	
	public PieceImpl(Player player, String pieceType)
	{
		this.player = player;
		this.pieceType = pieceType;
	}
	
	@Override
	public Player getPlayer() {
		return player;
	}

	@Override
	public BufferedImage getImage() {
		if (image == null && uiPlugin != null){
			image = uiPlugin.getPieceImage(pieceType);
		}
		return image;
	}

	@Override
	public void setImage(UIPlugin uiPlugin) {
		if(this.uiPlugin == null){
			this.uiPlugin = uiPlugin;
		}
		this.image = uiPlugin.getPieceImage(pieceType);
	}

	public String getPieceType(){
		return pieceType;
	}
	
	public void setPieceType(String pieceType){
		this.pieceType = pieceType;
		if(uiPlugin != null){
			image = uiPlugin.getPieceImage(pieceType);
		}
	}
	
}
