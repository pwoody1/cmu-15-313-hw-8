package implementations;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import interfaces.GameRunner;
import interfaces.plugins.*;
import interfaces.view.GamePicker;


/**
 * @author bcharna
 *
 */
public class GameRunnerImpl implements GameRunner {

	String selectedGamePlugin;
	JComboBox gamePluginsBox;
	JComboBox uIPluginsBox;
	JFrame topframe;

	@Override
	public void init() {
	    Enumeration<String> e = GamePluginFactory.gamePluginMappings.keys();
	    List<String> gamePluginNameList = new ArrayList<String>();
	    
	    while (e.hasMoreElements()){
	      String thisElement = e.nextElement();
		  gamePluginNameList.add(thisElement);
	    }

	    e = UIPluginFactory.uIPluginMappings.keys();
	    List<String> uIPluginNameList = new ArrayList<String>();
	    
	    while (e.hasMoreElements()){
	      String thisElement = e.nextElement();
		  
		  uIPluginNameList.add(thisElement);
	    }
	    Collections.sort(gamePluginNameList);
	    Collections.sort(uIPluginNameList);

	    createPicker(gamePluginNameList, uIPluginNameList);
	}
	
	@Override
	public void createPicker(List<String> gamePluginNameList, List<String> uiPluginNameList) {
		
	    topframe = new JFrame("Game Picker");
	    topframe.setSize(300, 150);
	    topframe.setLayout(new GridLayout(4,0));
	    gamePluginsBox = new JComboBox(gamePluginNameList.toArray());
	    
	    JLabel directions = new JLabel("Select a game and UI plugin.",JLabel.CENTER);
	    
        topframe.add("North", directions);
        topframe.add("Center", gamePluginsBox);
        
	    uIPluginsBox = new JComboBox(uiPluginNameList.toArray());
        topframe.add("Center", uIPluginsBox);

	    JButton okButton = new JButton("OK");
	    
	    okButton.addActionListener(new ActionListener() {
	    	 
            public void actionPerformed(ActionEvent e)
            {
                String gamePluginName = (String) gamePluginsBox.getSelectedItem();
                GamePlugin gamePlugin = GamePluginFactory.getGamePlugin(gamePluginName);
                GamePicker gamePicker = new GamePickerImpl();    

                String uIPluginName = (String) uIPluginsBox.getSelectedItem();
                UIPlugin uIPlugin = UIPluginFactory.getUIPlugin(uIPluginName);

                gamePicker.createLobby(gamePlugin, uIPlugin, gamePluginName);
                //topframe.setVisible(false); // hide the picker after user presses OK
            }
        });   
	    
        topframe.add("South", okButton);
	    topframe.setLocationRelativeTo(null);
	    topframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	    topframe.setVisible(true);
	}
	
	public static void main(String[] args) {
	    
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
        	    GameRunner gr = new GameRunnerImpl();
        	    gr.init();
            }
        });

	}
}