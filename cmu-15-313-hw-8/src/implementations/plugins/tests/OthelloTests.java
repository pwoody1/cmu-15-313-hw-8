package implementations.plugins.tests;

import static org.junit.Assert.*;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import implementations.model.PieceImpl;
import implementations.model.PlayerImpl;
import implementations.model.PositionImpl;
import implementations.plugins.Othello;
import implementations.plugins.OthelloUI;
import implementations.plugins.PlayerTag;
import interfaces.model.GameBoard;
import interfaces.model.Piece;
import interfaces.model.Player;
import interfaces.model.Position;
import interfaces.plugins.GamePlugin;

import org.junit.Before;
import org.junit.Test;

public class OthelloTests {
	
	private GamePlugin g;
	
	@Before
	public void beforeTest(){
		g = new Othello();
	}
	
	private boolean playerEquals(Player p1, Player p2){
		if ((p1 == null && p2 != null) || (p1 !=null && p2 == null)) return false;
		if(p1 == null && p2 == null) return true;
		return (p1.getName().equals(p2.getName()) && p1.getTeamNumber()==p2.getTeamNumber());		
	}
	
	private boolean pieceEquals(Piece p1, Piece p2){
		if ((p1 == null && p2 != null) || (p1 !=null && p2 == null)) return false;
		if(p1 == null && p2 == null) return true;
		return (p1.getPieceType().equals(p2.getPieceType()) && 
				playerEquals(p1.getPlayer(),p2.getPlayer()));
	}
	
	private boolean positionEquals(Position p1, Position p2){
		if ((p1 == null && p2 != null) || (p1 !=null && p2 == null)) return false;
		if(p1 == null && p2 == null) return true;
		return (pieceEquals(p1.getPiece(), p2.getPiece())
				&& (p1.getPoint().equals(p2.getPoint()))
				&& (p1.getTerrainType().equals(p2.getTerrainType())));
	}

	@Test
	public void setUpBoardTest() {
		Position[][] p = g.setupBoard(null);
		Piece blackPiece = new PieceImpl(new PlayerImpl("black",0), "Black");
		Piece whitePiece = new PieceImpl(new PlayerImpl("white",1), "White");
		for(int i = 0; i < 8; i ++){
			for(int j = 0; j < 8; j++){
				Position correctPosition = null;
				Position p1 = p[i][j];
				if((i == 3 && j == 3) ||(i==4 && j==4)){
					correctPosition = new PositionImpl(whitePiece,new Point(i,j),"default");
				}
				else if((i == 3 && j == 4) ||(i==4 && j==3)){
					correctPosition = new PositionImpl(blackPiece,new Point(i,j),"default");
				}
				else{
					correctPosition = new PositionImpl(null,new Point(i,j),"default");
				}
				assertTrue(positionEquals(p1, correctPosition));
			}
		}
	}
	
	@Test 
	public void whosTurnTest(){
		g.setupBoard(null);
		assertTrue(playerEquals(g.whosTurn(null),new PlayerImpl("black", 0)));
		g.runTurn("d 3", null);//move to directly above white piece.
		assertTrue(playerEquals(g.whosTurn(null),new PlayerImpl("white", 1)));
	}
	
	@Test
	public void gameOverBlackTest(){
		g.setupBoard(null);
		assertTrue(g.gameOver(null) == null);
		//shortest known game where black wins in a sweep
		String[] turnsBlack = {"e 6", "f 4", "e 3", "f 6", "g 5", "d 6", "e 7", "f 5", "c 5"};
		for( String turn : turnsBlack){
			g.runTurn(turn, null);
		}
		assertTrue(g.gameOver(null)[0] == 0);//black wins
	}
	
	@Test
	public void gameOverWhiteTest(){
		g.setupBoard(null);
		assertTrue(g.gameOver(null) == null);
		//shortest known game where white wins in a sweep
		String[] turnsWhite = {"d 3", "c 5", "e 6", "d 2", "c 4", "f 5", "c 6", "b 5", "d 6", "d 7"};
		for( String turn : turnsWhite){
			g.runTurn(turn, null);
		}
		assertTrue(g.gameOver(null)[0] == 1); //white wins
	}
	
	@Test
	public void runTurnTest(){
		g.setupBoard(null);
		GameBoard b = g.runTurn("d 3", null);
		Position[][] board = b.getBoard();
		PlayerImpl black = new PlayerImpl("black", 0);
		PlayerImpl white = new PlayerImpl("white", 1);
		for(int i = 0; i < 8; i ++){
			for (int j = 0; j < 8; j++){
				if((j == 3 && (i == 2 || i == 3 ||i == 4)) || (j == 4 && i == 3)){
					assertTrue(
					playerEquals(board[i][j].getPiece().getPlayer(),
							black));
				}
				else if(i ==4 && j ==4){
					assertTrue(
							playerEquals(board[i][j].getPiece().getPlayer(),
									white));
				}else{
					assertNull(board[i][j].getPiece());
				}
			}
		}
	}
	
	@Test
	public void getPieceImageWhiteTest(){
		try {
			BufferedImage expected = ImageIO.read(new File("./src/implementations/plugins/White.jpg"));
			OthelloUI u = new OthelloUI();
			BufferedImage actual = u.getPieceImage("White");
			for(int i = 0; i < expected.getWidth(); i ++){
				for(int j = 0; j < expected.getHeight(); j++){
					assertEquals(expected.getRGB(i, j), actual.getRGB(i,j));
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			fail();
		}
	}
	
	@Test
	public void getPieceImageBlackTest(){
		try {
			BufferedImage expected = ImageIO.read(new File("./src/implementations/plugins/Black.jpg"));
			OthelloUI u = new OthelloUI();
			BufferedImage actual = u.getPieceImage("Black");
			for(int i = 0; i < expected.getWidth(); i ++){
				for(int j = 0; j < expected.getHeight(); j++){
					assertEquals(expected.getRGB(i, j), actual.getRGB(i,j));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			fail();
		}
	}
	
	@Test
	public void getTerrainImageTest(){
		try {
			BufferedImage expected = ImageIO.read(new File("./src/implementations/plugins/tile.jpg"));
			OthelloUI u = new OthelloUI();
			BufferedImage actual = u.getTerrainImage("default");
			for(int i = 0; i < expected.getWidth(); i ++){
				for(int j = 0; j < expected.getHeight(); j++){
					assertEquals(expected.getRGB(i, j), actual.getRGB(i,j));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			fail();
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void pieceImageFailTest(){
		OthelloUI u = new OthelloUI();
		u.getPieceImage("green");
		fail();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void terrainImageFailTest(){
		OthelloUI u = new OthelloUI();
		u.getTerrainImage("green");
		fail();
	}
	
	

}
