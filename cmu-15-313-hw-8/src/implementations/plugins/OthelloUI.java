package implementations.plugins;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import interfaces.plugins.UIPlugin;

public class OthelloUI implements UIPlugin {
	private final String path = "./src/implementations/plugins/";

	@Override
	public BufferedImage getTerrainImage(String terrainType) {
		if(terrainType == null) throw new IllegalArgumentException("can't have null input");
		if(!terrainType.equals("default")) throw new IllegalArgumentException("only supports default terrain");
		BufferedImage in = null;
		try {
			in = ImageIO.read(new File(path+"tile.jpg"));
			return in;
		} catch (IOException e) {
			e.printStackTrace();
		}
		BufferedImage newImage = new BufferedImage(in.getWidth(), in.getHeight(), BufferedImage.TYPE_INT_ARGB);
		return newImage;
	}

	@Override
	public BufferedImage getPieceImage(String pieceType) {
		if(pieceType == null) throw new IllegalArgumentException("can't have null input");
		if(!(pieceType.equals("White") || pieceType.equals("Black"))){
			throw new IllegalArgumentException("only supports white and black pieces");
		}
		BufferedImage in = null;
		try {
			in = ImageIO.read(new File(path+pieceType+".jpg"));
			return in;
		} catch (IOException e) {
			e.printStackTrace();
		}
		BufferedImage newImage = new BufferedImage(in.getWidth(), in.getHeight(), BufferedImage.TYPE_INT_ARGB);
		return newImage;
	}
}
