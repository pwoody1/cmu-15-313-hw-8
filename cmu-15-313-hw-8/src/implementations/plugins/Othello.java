package implementations.plugins;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import implementations.model.*;
import implementations.panels.InfoPanelImpl;
import implementations.panels.InstructionsPanelImpl;
import implementations.panels.elements.TextLabel;
import interfaces.plugins.*;
import interfaces.model.*;
import interfaces.view.elements.InfoElement;
import interfaces.view.panels.*;

public class Othello implements GamePlugin {
        private PlayerTag turn = PlayerTag.BLACK;
        private final Piece blackPiece = new PieceImpl(new PlayerImpl("black",0), "Black");
        private final Piece whitePiece = new PieceImpl(new PlayerImpl("white",1), "White");
        private final int boardSize = 8;
        private GameBoard board = new SquareBoardImpl(setupBoard(null), boardSize, boardSize);
        private int numBlack = 2;
        private int numWhite = 2;

        @Override
        public int getMaxTeams() {
                return 2;
        }

        @Override
        public int getMinTeams() {
                return 2;
        }

        @Override
        public int getMaxPlayers() {
                return 2;
        }

        @Override
        public int getMinPlayers() {
                return 2;
        }

        @Override
        public int getMaxPerTeam() {
                return 1;
        }

        @Override
        public int getMinPerTeam() {
                return 1;
        }

        @Override
        public String[] computerTypes() {
                String[] ais = {"randomAI"};
                return ais;
        }

        public GameBoard makeAIMove(String computerType, GameState currentState){
                if (!computerType.equals("randomAI")) throw new IllegalArgumentException("AI not supported");
                List<Point> validMoves = getMoves(turn);
                Random r = new Random();
                int position = r.nextInt(validMoves.size());
                Point p = validMoves.get(position);
                String command = reverseMapping(p.x) + " " +(p.y +1);
                return runTurn(command, currentState);
                
        }
        @Override
        public InfoPanel makePositionInfoPanel(Position position) {
                InfoPanel retPanel = new InfoPanelImpl();
                InfoElement element;
                if(position.getPiece() != null)
                        element = new TextLabel(""+position.getPiece().getPieceType()+" piece selected.");
                else
                        element = new TextLabel("No piece selected.");
                retPanel.addElement(element);
                
                return retPanel;
        }

        @Override
        public InfoPanel makeGameInfoPanel(GameState currentState) {
                InfoPanel retPanel = new InfoPanelImpl();
                int yourNum = (whosTurn(null).getTeamNumber() == 0)? numBlack : numWhite;
                int oppNum = (whosTurn(null).getTeamNumber() == 0)? numWhite: numBlack;
                String retString = "<HTML>The score is:<BR>You: " + yourNum;
                retString += "<BR>" + "Opponent: " + oppNum;
                retPanel.addElement(new TextLabel(retString+"<BR>"+turn+"'s turn!</HTML>"));
                return retPanel;
        }

        @Override
        public InstructionsPanel makeInstructionsPanel(GameState currentState) {        
                
                String retString = "<HTML><BR>Issue moves of the form \"a 1\" where letters are the column and numbers are the rows, indexed at 1</HTML>";
                
                return new InstructionsPanelImpl(retString);
        }

        @Override
        public Position[][] setupBoard(Player[] players) {
                Position[][] positions = new Position[8][8];
                for(int i = 0; i < 8; i ++){
                        for(int j = 0; j < 8; j++){
                                positions[i][j] = new PositionImpl(null,new Point(i,j),"default");
                                if((i == 3 && j == 3)||(i==4 && j==4)){
                                        positions[i][j].setPiece(whitePiece);
                                }
                                else if((i == 3 && j == 4) ||(i==4 && j==3)){
                                        positions[i][j].setPiece(blackPiece);
                                }
                                
                        }
                }
                return positions;
        }

        /***
         * Swaps turn from BLACK -> WHITE or vice versa.
         */
        private void swapTurn() {
                turn = getOtherPlayerTag(turn);
        }
        
        /**
         * Evaluates the other player tag given a player tag.
         * @param tag
         * @return other player tag
         */
        private PlayerTag getOtherPlayerTag(PlayerTag tag) {
                switch(tag) {
                case BLACK:
                        return PlayerTag.WHITE;
                case WHITE:
                        return PlayerTag.BLACK;
                }
                return null;            
        }
        
        private int mapping(String s){
                if (s.equals("a")){
                        return 0;
                }
                else if(s.equals("b")){
                        return 1;
                }
                else if(s.equals("c")){
                        return 2;
                }
                else if(s.equals("d")){
                        return 3;
                }
                else if(s.equals("e")){
                        return 4;
                }
                else if(s.equals("f")){
                        return 5;
                }
                else if(s.equals("g")){
                        return 6;
                }else{
                        return 7;
                }               
        }
        private String reverseMapping(int i){
                switch(i){
                case 0: return "a";
                case 1: return "b";
                case 2: return "c";
                case 3: return "d";
                case 4: return "e";
                case 5: return "f";
                case 6: return "g";
                case 7: return "h";
                default: return null;
                }
        }
        @Override
        public GameBoard runTurn(String commandInput, GameState currentState) {
                String[] s = commandInput.split(" ");
                if (s.length != 2){
                        return null;
                }
                int x;
                int y;
                try{
                        x = mapping(s[0]);
                        y = Integer.parseInt(s[1]) - 1;
                }
                catch (Exception e){
                        return null;
                };
                if (!this.isInBounds(x) || !this.isInBounds(y)){
                        return null;
                }
                // (y,x) corresponds to (col,row)
                Point p = new Point(y,x);
                List<Point> moves = this.getMoves(turn);
                if (!moves.contains(p)){
                        return null;
                }
                // It's a legal move. So we make the move and switch turns
                makeMove(p);
                
                swapTurn();
                
                return board;
        }

        @Override
        public Player whosTurn(GameState currentState) {
                return getPlayerByTag(turn);
        }
        
        /**
         * Provides the Player associated with the input PlayerTag, tag.
         * @param tag
         * @return Player from tag
         */
        private Player getPlayerByTag(PlayerTag tag) {
                switch(tag){
                case BLACK:
                        return blackPiece.getPlayer();
                case WHITE:
                        return whitePiece.getPlayer();
                }
                return null;
        }
        
        /**
         * Provides the PlayerTag associated with the input Player.
         * @param player
         * @return tag from Player
         */
        private PlayerTag getTagByPlayer(Player player) {
                if(player == blackPiece.getPlayer()) {
                        return PlayerTag.BLACK;
                } else if(player == whitePiece.getPlayer()) {
                        return PlayerTag.WHITE;
                }
                return null;
        }
        
        private void swap(Point p) {
                Player player = board.getPositionAt(p).getPiece().getPlayer();
                swap(getTagByPlayer(player),p);
        }
        
        /**
         * Swaps the piece of tag at point p.
         * I.e. swap(BLACK, (0,1)) will make the piece at (0,1) white
         * Appropriately changes the number of pieces.
         * @param tag the tag of the player to be swapped
         * @param p point of the piece to swap
         */
        private void swap(PlayerTag tag, Point p){
                switch(tag){
                case BLACK:
                        numBlack--;
                        numWhite++;
                        board.getBoard()[p.x][p.y].setPiece(whitePiece);
                        break;
                case WHITE:
                        numBlack++;
                        numWhite--;
                        board.getBoard()[p.x][p.y].setPiece(blackPiece);
                        break;
                }
        }
        
        /***
         * Provides a list of all of the points that have the input player's
         *      pieces.
         * @param tag
         * @return list of all points with player's pieces
         */
        private List<Point> getPlayerPoints(PlayerTag tag) {
                List<Position> positions = board.getPositionsWithPieces();
                List<Point> points = new ArrayList<Point>();
                Player currPlayer = getPlayerByTag(tag);
                for(int i = 0; i < positions.size(); i++){
                        Position pos = positions.get(i);
                        if(pos.getPiece().getPlayer().equals(currPlayer)) {
                                // add it to our list
                                points.add(pos.getPoint());
                        }
                }
                return points;
        }
        
        /**
         * Evaluates whether the input v is within the board's bounds.
         * @param v
         * @return whether or not input in bounds
         */
        private boolean isInBounds(int v) {
                return v >= 0 && v < boardSize;
        }
        
        /**
         * Swaps all pieces in (start,end).  That is, exclusive.
         * Requires: that (start,end) are either diagonally connected or, if not
         *      fall on the same row or column.
         * @param start
         * @param end
         */
        private void swapInbetween(Point start, Point end) {
                // generate aggergate dx and dy to check for diag/horiz/vert
                int dxAggr = end.x - start.x;
                int dyAggr = end.y - start.y;
                if(!((start.x == end.x) || (start.y == end.y) ||
                                (Math.abs(dxAggr) == Math.abs(dyAggr)))) {
                        return;
                }
                // is a valid path, so swap!
                int dx = 0;
                int dy = 0;
                if(dxAggr > 0) {
                        dx = 1;
                } else if(dxAggr < 0) {
                        dx = -1;
                }
                
                if(dyAggr > 0) {
                        dy = 1;
                } else if(dyAggr < 0) {
                        dy = -1;
                }
                
                int numSwaps = Math.max(Math.abs(dxAggr), Math.abs(dyAggr)) - 1;
                for(int i = 1; i <= numSwaps; i++) {
                        Point currPoint = new Point(start.x + (i * dx), start.y + (i * dy));
                        swap(currPoint);
                }
        }
        
        /**
         * Given a valid move, point, this swaps all valid sequences of pieces that
         *      end at the provided point.
         * @param point
         */
        private void makeMove(Point point) {
                // get a list of all valid ending points from p
                List<Point> endPoints = new ArrayList<Point>();
                
                PlayerTag tag = turn;
                PlayerTag oppPlayerTag = getOtherPlayerTag(tag);
                for (int dx = -1; dx <= 1; dx++) {
                        for (int dy = -1; dy <= 1; dy++) {
                                if(dx == 0 && dy == 0) continue;
                                int currX = point.x + dx;
                                int currY = point.y + dy;
                                Point currPt = new Point(currX, currY);
                                if(!(isInBounds(currX) && isInBounds(currY))) continue;
                                Piece currPiece = board.getPositionAt(currPt).getPiece();
                                if(currPiece == null) continue;
                                if(getTagByPlayer(currPiece.getPlayer()) == oppPlayerTag) {
                                        // empty, so do a directional match for this player
                                        Point matched = direcMatchToPoint(currX, currY, dx, dy,
                                                        oppPlayerTag, true, tag);
                                        if(matched != null) {
                                                endPoints.add(matched);
                                        }
                                }
                        }
                }
                // set new piece
                switch(tag) {
                case BLACK:
                        board.getPositionAt(point).setPiece(blackPiece);
                        numBlack++;
                        break;
                case WHITE:
                        board.getPositionAt(point).setPiece(whitePiece);
                        numWhite++;
                        break;
                }
                
                // swap along paths for each of these points
                for (int i = 0; i < endPoints.size(); i++) {
                        Point endPoint = endPoints.get(i);
                        swapInbetween(point, endPoint);
                }
        }
        
        /**
         * Evaluates whether starting at some (x,y) it is possible to continue
         *      along (dx,dy) and find an (end) after some arbitrary contiguous number
         *      of (cont)s.  Boolean allows for requirement of at least one (cont) be
         *  found.  Reaching a null will always return false.
         * @param startX
         * @param startY
         * @param dx
         * @param dy
         * @param cont
         * @param atLeastOneCont
         * @param end
         * @return whether the directional match ended in an (end)
         */
        private boolean direcMatch(int startX, int startY, int dx, int dy,
                        PlayerTag cont, boolean atLeastOneCont, PlayerTag end) {
                return (direcMatchToPoint(startX, startY, dx, dy, cont, atLeastOneCont,
                                end)) != null;
        }
        
        /**
         * Evaluates whether starting at some (x,y) it is possible to continue
         *      along (dx,dy) and find an (end) after some arbitrary contiguous number
         *      of (cont)s.  If a point is found, it returns the point.  Otherwise,
         *      returns null.
         * @param startX
         * @param startY
         * @param dx
         * @param dy
         * @param cont
         * @param atLeastOneCont
         * @param end
         * @return the point the directMatch ends at, if applicable.  Null
         *      otherwise.
         */
        private Point direcMatchToPoint(int startX, int startY, int dx, int dy,
                        PlayerTag cont, boolean atLeastOneCont, PlayerTag end) {
                int currX = startX;
                int currY = startY;
                boolean foundACont = false;
                while(isInBounds(currX) && isInBounds(currY)) {
                        Point currPt = new Point(currX, currY);
                        Piece currPiece = board.getPositionAt(currPt).getPiece();
                        if(currPiece == null) {
                                // we hit a snag!  It's empty...
                                return null;
                        } else {
                                Player currPlayer = currPiece.getPlayer();
                                PlayerTag currPlayerTag = getTagByPlayer(currPlayer);
                                if (currPlayerTag == end) {
                                        // we are finished!
                                        if( !(atLeastOneCont) || foundACont ) {
                                                return currPt;
                                        } else {
                                                return null;
                                        }
                                } else if(currPlayerTag == cont) {
                                        if(!foundACont) foundACont = true;
                                        currX += dx;
                                        currY += dy;
                                }
                        }
                }
                return null;
        }
        
        /**
         * Evaluates all legal moves the player corresponding to the input tag
         *      is able to move to.
         * @param tag
         * @return list of points player can move to
         */
        private List<Point> getMoves(PlayerTag tag){
                if(tag == null) {
                        return null;
                }
                List<Point> validMoves = new ArrayList<Point>();
                
                PlayerTag oppPlayerTag = getOtherPlayerTag(tag);
                List<Point> oppPoints = getPlayerPoints(oppPlayerTag);
                for (int i = 0; i < oppPoints.size(); i++) {
                        Point point = oppPoints.get(i);
                        // determine which directions are valid
                        for (int dx = -1; dx <= 1; dx++) {
                                for (int dy = -1; dy <= 1; dy++) {
                                        if(dx == 0 && dy == 0) continue;
                                        int currX = point.x + dx;
                                        int currY = point.y + dy;
                                        Point currPt = new Point(currX, currY);
                                        if(!isInBounds(currX) && isInBounds(currY)) continue;
                                        Position pos = board.getPositionAt(currPt);
                                        if(pos == null) continue;
                                        if(pos.getPiece() == null) {
                                                // empty, so do a directional match for this player
                                                if(direcMatch(currX - dx, currY - dy, -dx, -dy,
                                                                oppPlayerTag, true, tag)) {
                                                        validMoves.add(currPt);
                                                }
                                        }
                                }
                        }
                }
                return validMoves;
        }
        @Override
        public int[] gameOver(GameState currentState) {
                if(getMoves(PlayerTag.BLACK).size() == 0 && getMoves(PlayerTag.WHITE).size() == 0){
                        return (numBlack > numWhite) ? new int[]{0}: new int[]{1};
                }
                return null;
        }

        @Override
        public GameBoard cleanUp(GameState currentState) {
                return currentState.getGameBoard();
        }

        
        /* ------------------------------------------------------------------------
         * The following methods are not needed but are here to meet the interface.
         * They will never be called.
         * ------------------------------------------------------------------------
         */
        @Override
        public void setMaxTeams(int maxTeams) {
                // TODO Auto-generated method stub
                
        }

        @Override
        public void setMinTeams(int minTeams) {
                // TODO Auto-generated method stub
                
        }

        @Override
        public void setMaxPlayers(int maxPlayers) {
                // TODO Auto-generated method stub
                
        }

        @Override
        public void setMinPlayers(int minPlayers) {
                // TODO Auto-generated method stub
                
        }

        @Override
        public void setMaxPerTeam(int maxPerTeam) {
                // TODO Auto-generated method stub
                
        }

        @Override
        public void setMinPerTeam(int minPerTeam) {
                // TODO Auto-generated method stub
                
        }

        @Override
        public void setComputerTypes(String[] computerTypes) {
                // TODO Auto-generated method stub              
        }

		@Override
		public GameBoard runAi(GameState state) {
			// TODO Auto-generated method stub
			return null;
		}


}
