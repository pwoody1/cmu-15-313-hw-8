package implementations.plugins;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import interfaces.model.GameState;
import interfaces.model.Piece;
import interfaces.plugins.UIPlugin;
import interfaces.view.panels.InfoPanel;
import interfaces.view.panels.InstructionsPanel;

public class PokeRacerUI implements UIPlugin {

	private final String path = "./src/implementations/plugins/PokeRacerPictures/";
	@Override
	public BufferedImage getTerrainImage(String terrainType) {
		if(terrainType == null)
			return null;
		if(terrainType.equals("Goal"))
		{
			BufferedImage img = null;
			try{
				img = ImageIO.read(new File(path + "Goal.jpg"));
				return img;
			}catch(IOException e)
			{
				return null;
			}
		}
		return null;
	}

	@Override
	public BufferedImage getPieceImage(String pieceType) {
		if(pieceType == null)
			return null;
		if(pieceType.equals("Trainer"))
		{
			File file = new File(path + "trainer.jpg");

			try{
				return ImageIO.read(file);
			}catch(IOException e){
				try {
					System.out.println("Hello" + file.getCanonicalPath());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return null;
			}
		}
		else if(pieceType.equals("Rival"))
		{
			try{
				return ImageIO.read(new File(path + "rival.jpg"));
			}catch(IOException e){
				return null;
			}
		}
		else
			return null;
	}

}
