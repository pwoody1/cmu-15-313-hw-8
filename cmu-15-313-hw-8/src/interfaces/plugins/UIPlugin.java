package interfaces.plugins;

import java.awt.Color;
import java.awt.image.BufferedImage;


/**
 * An implementation of the UIPlugin interface represents the visuals and UI
 *  elements that represent that state of the game board.
 *  
 * Every UIPlugin should implement a method that displays the vector on the 
 *   screen.
 * Likewise every UIPlugin should change the visuals in sync with updates 
 *   to the game state. The state of the game is held in the framework, through
 *   the GameFrame class.
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface UIPlugin {

	/**
	 * Given a String representing a terrain type, returns a BufferedImage
	 * that will be painted on the background of any position with 
	 * this terrain type. Must handle null strings as the 
	 * "default" terrain type.
	 * @param terrainType A string representing a terrain's type.
	 * @return A BufferedImage representing the terrain
	 */
	
	public BufferedImage getTerrainImage(String terrainType);
	
	/**
	 * Given a String representing a piece type, returns an image
	 * that will be painted on to the position containing the piece.
	 * @param pieceType A string representing a piece's type.
	 * @return A BufferedImage representing the piece in question
	 */
	
	public BufferedImage getPieceImage(String pieceType);

}
