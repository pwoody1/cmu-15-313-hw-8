package interfaces.model;


/**
 *  This class defines the specifics for a board that has at most 6 adjacent
 *  locations.
 *  
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface HexBoard extends GameBoard{

}
