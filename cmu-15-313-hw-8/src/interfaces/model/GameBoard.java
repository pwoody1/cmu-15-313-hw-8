package interfaces.model;
import java.awt.Point;
import java.util.List;

/**
 * This class is intended to simply implement some functionalities of the
 *   board that the players are playing on. It holds the state of the board
 *   such as which players turn it is and provides some convenience functions
 *   that allow the user to get points adjacent to a specific point and get 
 *   the {@link Piece} at a specific point.
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 *
 */
public interface GameBoard {
	
	/* Also contains height and width properties*/
		
	public Position[][] getBoard();
	
	/**
	 * Maps a point to a unique identifier for a position.
	 * @param point Position's point .
	 * @return Unique identifier.
	 */
	
	public String pointToUID(Point point);
	
	/**
	 * Maps a unique identifier to a position's point
	 * @param point Position's unique identifier
	 * @return A Position's point 
	 */
	
	public Point pointFromUID(String uid);
	
	public int getWidth();
	
	public int getHeight();
	
	
	/**
	 * Assigns a BufferedImage to the given piece
	 * using the method in the UIPlugin. Then adds 
	 * Piece piece to the Position corresponding
	 * to the Point point.
	 * @param p Position that we are adding to the board.
	 */
	public void addNewPosition(Position pos);
	
	/**
	 * Gets the list of {@link Position} objects that are on the board.
	 * @return a list of {@link Position} objects that are on the board
	 */
	 List<Position> getPositionsWithPieces();

	/**
	 * This method is meant to, given a {@link Point} return the list of points
	 *   that are considered adjacent to it. In a grid that would be the points
	 *   that are above, below, right, and left of the specific {@link Point}
	 * @param srcPoint	the source point that you want to find the adjacent 
	 *   points to.
	 * @return all of the points that are considered "adjacent" to the 
	 *   specified point
	 */
	List<Point> getAdjacent(Point srcPoint);
	
	/**
	 * Gets the {@link Position} that is at the specified {@link Point}.
	 * Returns null if no pieces are found.
	 * @param point	location of the {@link Point} that you wish to find.
	 * @return the {@link Position} that is at the specified {@link Point}, 
	 *   otherwise null
	 */
	Position getPositionAt(Point point);
	
}
